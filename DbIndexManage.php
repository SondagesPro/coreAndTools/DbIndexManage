<?php

/**
 * DbIndexManage
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class DbIndexManage extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Manage some db index.';
    protected static $name = 'DbIndexManage';
    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'permission_read' => array(
            'type' => 'checkbox',
            'label' => 'Index on table permission: read',
            'table' => 'permissions',
            'columns' => 'read_p',
            'default' => 0
        ),
        /* Survey->search */
        'permission_entity_uid_read' => array(
            'type' => 'checkbox',
            'label' => 'Index on table permission: entity + entity_id + uid + permission +read',
            'table' => 'permissions',
            'columns' => 'entity,entity_id,uid,permission,read_p',
            'unique' => true,
            'default' => 0
        ),
        'plugins_name' => array(
            'type' => 'checkbox',
            'label' => 'Index on table plugins  : name',
            'table' => 'plugins',
            'columns' => 'name',
            'unique' => true,
            'default' => 0
        ),
        'plugins_active' => array(
            'type' => 'checkbox',
            'label' => 'Index on table plugins  : active',
            'table' => 'plugins',
            'columns' => 'active',
            'default' => 0
        ),
        'question_attributes_qid_attribute' => array(
            'type' => 'checkbox',
            'label' => 'Index on table question_attributes  : qid and attribute',
            'table' => 'question_attributes',
            'columns' => 'qid,attribute',
            'default' => 0
        ),
        'question_attributes_attribute_value_100' => array(
            'type' => 'checkbox',
            'label' => 'Index on table question_attributes  : attribute and value (100 characters)',
            'table' => 'question_attributes',
            'columns' => 'attribute,value (100)',
            'default' => 0
        ),
    );
    /** @inheritdoc **/
    public function init()
    {
        /* No subscription : only settings */
    }

     /** @inheritdoc **/
    public function getPluginSettings($getValues = true)
    {
        if (!self::getIsForcedSuperAdmin()) {
            return array();
        }
        $pluginSettings = parent::getPluginSettings($getValues);
        return $pluginSettings;
    }

    /**
     * @inheritdoc
     * and set menu if needed
    **/
    public function saveSettings($settings)
    {
        if (!self::getIsForcedSuperAdmin()) {
            throw new CHttpException(403);
        }
        $DBsettings = parent::getPluginSettings(true);
        foreach ($this->settings as $name => $data) {
            $newSetting = boolval(App()->getRequest()->getPost($name));
            $oldSetting = !empty($DBsettings[$name]['current']);
            if($newSetting !== $oldSetting) {
                $tableName = '{{' . $data['table'] . '}}' ;
                $colums = $data['columns'];
                $unique = !empty($data['unique']);
                $indexname = 'dbindexmanage_' . $name;
                if($newSetting) {
                    try {
                        App()->getDb()->createCommand()->createIndex($indexname, $tableName, $colums, $unique);
                    } catch (Exception $e) {
                        // index surely already exist ?
                        App()->setFlashMessage($e->getMessage(), 'warning');
                    }
                } else {
                    try {
                        App()->getDb()->createCommand()->dropIndex($indexname, $tableName);
                    } catch (Exception $e) {
                        // index surely already exist ?
                        App()->setFlashMessage($e->getMessage(), 'warning');
                    }
                }
            }
        }
        parent::saveSettings($settings);
    }

    /**
     * Get if is forced super admin
     * @return booelan
     */
    private static function getIsForcedSuperAdmin()
    {
        if (Yii::app() instanceof CConsoleApplication) {
            return true;
        }
        if(method_exists('Permission','getUserId')) {
            $userId = Permission::model()->getUserId();
        } else {
            $userId = Permission::getUserId();
        }
        return Permission::isForcedSuperAdmin($userId);
    }
}
