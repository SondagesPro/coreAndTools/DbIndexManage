# DbIndexManage

Manage some db index.

## Usage

Settings access is allowed only to forced admin user, here can activate or deactivate some DB index.

## Copyright
- Copyright © 2022 Denis Chenu / SondagesPro <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>

## Support
- Issues and pull request <https://gitlab.com/SondagesPro/coreAndTools/DbIndexManage>
- Professional support <https://www.sondages.pro/contact.html>
